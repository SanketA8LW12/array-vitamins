const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 


// 1. Get all items that are available 

function getAvailableItems(items){
    if(items.available === true){
        return items;
    }
}

let allAvailableItems = items.filter(getAvailableItems);

console.log(allAvailableItems);


// 2. Get all items containing only Vitamin C.

function getItemsWithVitamin_C(items){
    if(items.contains === 'Vitamin C'){
        return items;
    }
}

let itemsWithVitamin_C = items.filter(getItemsWithVitamin_C);

console.log(itemsWithVitamin_C);

// 3. Get all items containing Vitamin A.

function getItemsWithVitamin_A(items){
    if(items.contains.includes('Vitamin A')){
        return items;
    }
}

let itemsWithVitamin_A = items.filter(getItemsWithVitamin_A);

console.log(itemsWithVitamin_A);


// 4. Group items based on the Vitamins that they contain in the following format:
//         {
//             "Vitamin C": ["Orange", "Mango"],
//             "Vitamin K": ["Mango"],
//         }
        
//         and so on for all items and all Vitamins.

let vitaminObj = {};

function addKeyToVitaminObj(items){

    let vitaminArr = items.contains.split(',');

    function pushItemToVitaminObject(fruits){
        
        if(!vitaminObj[fruits.trim()]){
            vitaminObj[fruits.trim()] = [];
        }
        vitaminObj[fruits.trim()].push(items.name);
    }

    vitaminArr.map(pushItemToVitaminObject);
   
}

items.map(addKeyToVitaminObj);

console.log(vitaminObj);



// 5. Sort items based on number of Vitamins they contain.
